<h1 align="center">
📄<br>README Template
</h1>

### Hi there, This is Vinh Duong 😎

## I'm a IT guys!
- 🔭 I’m currently studying as LAB University of Applied Sciences!
- 🌱 I’m currently learning Industial Information Technology!
- ⚡ Fun fact: I love to listen music!


<br />

### Languages and Tools:


![HTML](https://img.shields.io/badge/-HTML-05122A?style=flat&logo=HTML5)&nbsp;
![Git](https://img.shields.io/badge/-Git-05122A?style=flat&logo=git)&nbsp;
![GitHub](https://img.shields.io/badge/-GitHub-05122A?style=flat&logo=github)&nbsp;
![Gitlab](https://img.shields.io/badge/-Gitlab-05122A?style=flat&logo=gitlab)&nbsp;
![Python](https://img.shields.io/badge/-Python-05122A?style=flat&logo=python)&nbsp;
![JavaScript](https://img.shields.io/badge/-JavaScript-05122A?style=flat&logo=javascript)&nbsp;
![React](https://img.shields.io/badge/-React-05122A?style=flat&logo=react)&nbsp;
![Angular](https://img.shields.io/badge/-Angular-05122A?style=flat&logo=angular)&nbsp;
![TypeScript](https://img.shields.io/badge/-Typescript-05122A?style=flat&logo=typescript)&nbsp;
![Node.js](https://img.shields.io/badge/-Node.js-05122A?style=flat&logo=node.js)&nbsp;
![MongoDB](https://img.shields.io/badge/-MongoDB-05122A?style=flat&logo=mongodb)&nbsp;
![MySQL](https://img.shields.io/badge/-MySQL-05122A?style=flat&logo=mysql)&nbsp;
![Redis](https://img.shields.io/badge/-Redis-05122A?style=flat&logo=redis)&nbsp;
![Docker](https://img.shields.io/badge/-Docker-05122A?style=flat&logo=docker)&nbsp;
![Jasmine](https://img.shields.io/badge/-Jasmine-05122A?style=flat&logo=jasmine)&nbsp;

<br />

---

### 📕 Latest Blog Posts
<!-- BLOG-POST-LIST:START -->
- [Markdown guide](https://www.markdownguide.org/cheat-sheet/)
<!-- BLOG-POST-LIST:END -->

---

## Images
![Stormtroopocat](https://octodex.github.com/images/stormtroopocat.jpg "The Stormtroopocat")